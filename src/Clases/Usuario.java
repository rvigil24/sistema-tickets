/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Ruben
 */
public class Usuario extends Persona {
    protected int idUsuario;
    protected String nomUsuario;
    protected String passUsuario;
    protected String fechaCreacion;
    protected int codRol;
    
    
    public Usuario(){
        
    }
    
    public Usuario(String primerNom, String primerApe){
        super(primerNom, primerApe);
    }    
    
    public Usuario(String primNom, String segNom, String primApe, String segApe,
                   String fecNac, String genPersona, Long dui, Long tel, String dir){
        super(primNom, segNom, primApe, segApe, fecNac, genPersona, dui, tel, dir );

    }        
    
    
    //========  SETTERS ================
    
    public void setidUsuario(int id){
        this.idUsuario = id;
    }
    
    public void setnomUsuario(String nomUsuario){
        this.nomUsuario = nomUsuario;
    }
    
    public void setPassUsuario(String pass){
        this.passUsuario = pass;
    }
    
    public void setFecCreacion(String fecha){
        this.fechaCreacion = fecha;
    }
    
    public void setCodRol(int codRol){
        this.codRol = codRol;
    }
    
    //========  GETTERS ================
    
    public int getIdUsuario(){
        return this.idUsuario;
    }
    
    public String getNomUsuario(){
        return this.nomUsuario;
    }
    
    public String getPassUsuario(){
        return this.passUsuario;
    }    
    
    public String getFecCreacion(){
        return this.fechaCreacion;
    }    
    
    public int getCodRol(){
        return this.codRol;
    }    
    
}
    