/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import java.sql.*;

/**
 *
 * @author Ruben
 */
public class Conexion {
    static Propiedades props = new Propiedades();
    static Connection conn;
    static String DBUser = props.getDBProps().getProperty("DBUser");
    static String DBPassword = props.getDBProps().getProperty("DBPassword");
    static String DBUrl = props.getDBProps().getProperty("DBUrl");
    
    public static Connection conectar(){
        try{
            Class.forName(props.getDBProps().getProperty("DB_Driver"));
            conn = DriverManager.getConnection( DBUrl, DBUser, DBPassword);
        }
        catch(Exception ex){
            String error = ex.getMessage();
        }
        return conn;
    }
    
    
}
