/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
import Clases.Conexion;
import Clases.Usuario;
import Clases.Cliente;
import java.sql.*;

/**
 *
 * @author Ruben
 */
public class Administrador {
     
    private Connection cnn;
     private Usuario user;
     private Cliente cliente;
     
     public Administrador(){
         this.user = new Usuario();
         this.cliente = new Cliente();
         
     }
     
     //metodos crear
     public void crearAdministrador(){
         
     }
     
     public void crearSupervisor(){
         
     }
     
     public void crearAgente(){
         
     }
     
     public void crearCliente(){
         
     }
     
     public void crearTicket(){
         
     }
     
     public void crearCentro(){
         
     }
     
     public void crearPrioridad(){
         
     }
     
     
     //metodos editar
     public void editarAdministrador(){
         
     }
     
     public void editarSupervisor(){
         
     }
     
     public void editarAgente(){
         
     }
     
     public void editarCliente(){
         
     }
     
     public void editarTicket(){
         
     }
     
     public void editarCentro(){
         
     }
     
     public void editarPrioridad(){
         
     }
     

     //metodos consultar
     public void econsultarAdministrador(){
         
     }
     
     public void consultarSupervisor(){
         
     }
     
     public void consultarAgente(){
         
     }
     
     public void consultarCliente(){
         
     }
     
     public void consultarTicket(){
         
     }
     
     public void consultarCentro(){
         
     }
     
     public void consultarPrioridad(){
         
     }          
     
     
     //metodos eliminar
     public void eliminarAdministrador(){
         
     }
     
     public void eliminarSupervisor(){
         
     }
     
     public void eliminarAgente(){
         
     }
     
     public void eliminarCliente(){
         
     }
     
     public void eliminarTicket(){
         
     }
     
     public void eliminarCentro(){
         
     }
     
     public void eliminarPrioridad(){
         
     }     
     
     
     //==========   METODOS ===========
     
     public void crearPersona(){
         cnn = Conexion.conectar();
         try{
             
             
         }
         catch(Exception ex){
             
         }
     }
     
     public void setUsuario(Usuario user){
         this.user = user;
     }
     
     public void setCliente(Cliente cliente){
         this.cliente   = cliente;
     }
     
     public Usuario getUsuario(){
         return this.user;
     }
     
     public Cliente getCliente(){
         return this.cliente;
     }
     
     
}
