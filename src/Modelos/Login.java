/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
import Clases.Conexion;
import java.sql.*;
import java.util.HashSet;

/**
 *
 * @author Ruben
 */
public class Login {
    private String username;
    private String password;
    private Connection cnn;
    
    public Login(){
    }
    
    public void setUsername(String user){
        this.username = user;
    }
    
    public  void setPassword(String pass){
        this.password = pass;
    }
    
    public ResultSet getLogin(){
        cnn = Conexion.conectar();
        ResultSet rs = null;
        String checkLogin = "CALL login_usuario(?,?)";
        try{
            CallableStatement stmt= this.cnn.prepareCall(checkLogin);
            stmt.setString( 1, this.username);
            stmt.setString( 2, this.password);
            rs = stmt.executeQuery();
        }
        catch(Exception ex){
            
        }
        return rs;
    }
    
    public ResultSet getRol(String user){
        cnn = Conexion.conectar();
        ResultSet rs = null;
        String queryGetRol = "CALL get_rol_usuario(?)";
        try{
            CallableStatement stmt = cnn.prepareCall( queryGetRol );
            stmt.setString( 1, user);
            rs = stmt.executeQuery();
        }
        catch(Exception ex){
            
        }
        return rs;
    }
    
    public Boolean comprobarCredenciales(String usuario, String pass){
        Boolean credencialesValidas = true;
        if( usuario.equals( "") || pass.equals( "")){
            credencialesValidas = false;
        }
        return credencialesValidas;
    }    
    
}
